'use strict';


angular.module('forms').directive('submitFormDirective', ['$http', 'TimeCounter', '$filter', '$rootScope', 'Auth', '$interval',
    function ($http, TimeCounter, $filter, $rootScope, Auth, $interval) {
        return {
            templateUrl: 'modules/forms/base/views/directiveViews/form/submit-form.client.view.html',
			restrict: 'E',
            scope: {
                myform:'='
            },
            controller: function($document, $window, $scope){
                $scope.authentication = $rootScope.authentication;
		        $scope.noscroll = false;
                $scope.forms = {};
                $scope.showCondQuestionChange = $rootScope.showCondQuestionChange = function(x){
                    if ($rootScope.showCondQuestion === 'false'){
                        $scope.myform.form_fields[1].fieldValue = 'Não';
                    }
                }

                _.each($scope.myform.form_fields, function(f, i){
                    f.index = i;
                    f.visible = true;
                });

                $("#loading").hide();


                window.onscroll = function(e){
                    var  scrOfY = 0;
                    if( typeof( window.pageYOffset ) == 'number' ) {
                        //Netscape compliant
                        scrOfY = window.pageYOffset;

                    } else if( document.body && document.body.scrollTop )  {
                        //DOM compliant
                        scrOfY = document.body.scrollTop;
                    } 

                    if(scrOfY > 785){
                        jQuery("#barra-legenda").addClass("question-group-bar");
                        $('#barra-legenda').css('top', $('.form-brand-header').height() + 'px');
                    }else{
                        jQuery("#barra-legenda").removeClass("question-group-bar");
                        $('#barra-legenda').css('top', '0');
                    }
                }

                var grouped = _.groupBy($scope.myform.form_fields, 'description');
                var sorted = Object.keys(grouped, function(a, b) {
                    var x = grouped[a][0].index;
                    var y = grouped[b][0].index; 
                    return x < y ? -1 : x > y ? 1 : 0;
                });

                $scope.groupedFormFields = _.map(sorted, function(key){
                    var name = key;
                    var description = "";

                    var split = key.split(":");
                    if(split.length == 2){
                        name = split[0];
                        description = split[1];
                    }

                    return {
                        name: name,
                        description: description,
                        fields: grouped[key]
                    };
                });

				var form_fields_count = $scope.myform.visible_form_fields.filter(function(field){
                    if(field.fieldType === 'statement' || field.fieldType === 'rating'){
                        return false;
                    }
                    return true;
                }).length;

				var nb_valid = $filter('formValidity')($scope.myform);

				$scope.translateAdvancementData = {
					done: nb_valid,
					total: (form_fields_count - 2),
					answers_not_completed: (form_fields_count - 2) - nb_valid
				};

                $scope.reloadForm = function(){
                    //Reset Form
                    $scope.myform.submitted = false;
                    $scope.myform.form_fields = _.chain($scope.myform.visible_form_fields).map(function(field){
                            field.fieldValue = '';
                            return field;
                        }).value();

					$scope.loading = false;
                    $scope.error = '';

                    //Reset Timer
                    TimeCounter.restartClock();
                };


                $scope.setActiveField = $rootScope.setActiveField = function(field_id, field_index, animateScroll) {
					var nb_valid = $filter('formValidity')($scope.myform);
					$scope.translateAdvancementData = {
						done: nb_valid,
						total: form_fields_count - 2,
						answers_not_completed: (form_fields_count - 2) - nb_valid
					};
                };

                $scope.unsuccessfulAttemps = 0;

                $scope.exitStartPageOnEnterPress = function(e){
                    if (e.keyCode == 13) {
                        $scope.exitStartPage();
                    }
                }

                /*
                ** Form Display Functions
                */
                $scope.exitStartPage = function(){
                    if(grecaptcha){
                        $scope.access.captcha = grecaptcha.getResponse();
                    }

                    $scope.access.code = $scope.access.code.trim();
                    $scope.access.password = $scope.access.password.trim();
                    
                    $http.post('/respondent/auth/' + $scope.myform._id, $scope.access)
                    .success(function (data, status, headers) {
                        $scope.myform.respondentId = data.respondent._id;

                        if(data.submission){
                            $scope.myform.prevSubmission = data.submission._id;

                            var submissionFields = _.groupBy(data.submission.form_fields, "submissionId");
                            $scope.myform.form_fields.forEach(function (formField){
                                var value = submissionFields[formField._id][0].fieldValue;
                                formField.fieldValue = value;
                            });
                        }

                        $scope.myform.startPage.showStart = false;

                        setTimeout(function() {
                            $('.form-fields').css('margin-top', $('.form-brand-header').height() + 'px');
                        }, 100);
                        
                    })
                    .error(function (error) {
                        $scope.unsuccessfulAttemps++;
                        $scope.loading = false;
                        $scope.error = error.msg;
                    });                    
                };


				$rootScope.submitForm = $scope.submitForm = function(partial, confirmed) {
                    var form = _.cloneDeep($scope.myform);

					// form.percentageComplete = $filter('formValidity')($scope.myform) / ($scope.myform.visible_form_fields.length - 1) * 100;
					// if(!partial && form.percentageComplete < 100 && !confirmed){
                    //      $("#confirmar-finalizacao-modal").modal('show');
                    //      return;
                    // }else {
                    //     $("#confirmar-finalizacao-modal").modal('hide')
                    // }

                    delete form.visible_form_fields;
                    delete form.submissions

                    var _timeElapsed = TimeCounter.stopClock();
                    
                    $scope.partial = partial;
                    if(partial){
                        $scope.partialLoading = true;    
                    }else{
                        $scope.loading = true;
                    }

					form.timeElapsed = _timeElapsed;
                    form.partial = partial;
                    form.respondentId = $scope.myform.respondentId;

					for(var i=0; i < $scope.myform.form_fields.length; i++){
						if($scope.myform.form_fields[i].fieldType === 'dropdown' && !$scope.myform.form_fields[i].deletePreserved){
							$scope.myform.form_fields[i].fieldValue = $scope.myform.form_fields[i].fieldValue.option_value;
						}
					}

					setTimeout(function () {
						$scope.submitPromise = $http.post('/forms/' + $scope.myform._id, form)
							.success(function (data, status, headers) {
								$scope.myform.submitted = true;
                                if(partial){
                                    $scope.partialLoading = false;    
                                }else{
                                    $scope.loading = false;
                                }
							})
							.error(function (error) {
                                if(partial){
                                    $scope.partialLoading = false;    
                                }else{
                                    $scope.loading = false;
                                }
								console.error(error);
								$scope.error = error.message;
							});
					}, 500);
                };

				$scope.reloadForm();
            }
        };
    }
]);
