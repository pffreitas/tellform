'use strict';

angular.module('forms').config(['$translateProvider', function ($translateProvider) {

  $translateProvider.translations('en', {
    FORM_SUCCESS: 'Formulário enviado com sucesso!',
	REVIEW: 'Revisar',
    BACK_TO_FORM: 'Voltar ao formulário',
	EDIT_FORM: 'Editar Formulário',
	CREATE_FORM: 'Criar Formulário',
	ADVANCEMENT: '{{done}} de {{total}} perguntas respondidas',
	CONTINUE_FORM: 'Continuar',
	REQUIRED: 'obrigatório',
	COMPLETING_NEEDED: '{{answers_not_completed}} perguntas precisam ser respondidas',
	OPTIONAL: 'opcional',
	ERROR_EMAIL_INVALID: 'Por favor, informe um e-mail válido',
	ERROR_NOT_A_NUMBER: 'Por favor, informe apenas números',
	ERROR_URL_INVALID: 'Por favor, informe uma URL válida',
	OK: 'OK',
	ENTER: 'ENTER',
	YES: 'Sim',
	NO: 'Não',
	NEWLINE: 'SHIFT+ENTER para criar uma nova linha',
	CONTINUE: 'Continuar',
	LEGAL_ACCEPT: 'Eu aceito',
	LEGAL_NO_ACCEPT: 'Eu não aceito',
	DELETE: 'Apagar',
	CANCEL: 'Cancela',
	SUBMIT: 'Enviar',
	UPLOAD_FILE: 'Upload'
  });
	
}]);
