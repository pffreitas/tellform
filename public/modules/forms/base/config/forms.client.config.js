'use strict';

// Configuring the Forms drop-down menus
angular.module('forms').value('supportedFields', [
	'textfield',
	'textarea',
	'date',
	'dropdown',
	'hidden',
	'password',
	'radio',
	'legal',
	'statement',
	'rating',
	'yes_no',
	'number',
	'natural',
	'two_answers',
	'twoa_radio',
	'resp_info',
	'resp_info_2',
	'checkbox'
]);
