'use strict';

angular.module('forms').directive('editSubmissionsFormDirective', ['$rootScope', '$http', 'Forms', 'Submissions', 'Stats', '$stateParams', '$interval', 'ResponsesByServico',
    function ($rootScope, $http, Forms, Submissions, Stats, $stateParams, $interval, ResponsesByServico) {
        return {
            templateUrl: 'modules/forms/admin/views/directiveViews/form/edit-submissions-form.client.view.html',
            restrict: 'E',
            scope: {
                user:'=',
				myform: '='
            },
            controller: function($scope){
                $scope.selectedServico = "";
                $scope.servicos = [];

                $scope.table = {
                    masterChecker: false,
                    rows: []
                };

                $scope.table.rows = [];
                
                $scope.AverageTimeElapsed = 0;
                $scope.ResponseCount = 0;
                $scope.LoginCount = 0;

                $scope.Chart = c3.generate({
                    bindto: '#chart',
                    axis: {
                        x: {
                            type: 'timeseries',
                            tick: {
                                format: '%Y-%m-%d'
                            }
                        }
                    },                    
                    data: {
                        x: 'x',
                        type: "bar",
                        columns: []
                    }
                });       
                
                $scope.Chart3 = c3.generate({
                    bindto: '#chart3',
                    data: {
                        type: "pie",
                        columns: []
                    }
                });            
                
                $scope.changeSelectedServico = function(x){
                    $scope.generateQualidadeServicoChart($scope.qualidadeServicoChartData);
                }

                $scope.generateQualidadeServicoChart = function(response){
                    var dim = ["Péssimo","Ruim","Regular","Bom","Ótimo"];
                    var servicos = _.keys(_.groupBy(response, "_id.servico"));
                    $scope.servicos = servicos;
                    if(!$scope.selectedServico){
                        $scope.selectedServico = servicos[0];
                    }

                    servicos = _.filter(servicos, function(s) {
                        return s === $scope.selectedServico;
                    });

                    var groupedByDimAndServico = _.groupBy(response, function(d){
                        return d._id.value +"_"+d._id.servico;
                    });

                    var data = _.map(dim, function(d){
                        var values = _.map(servicos, function(servico){
                            if (groupedByDimAndServico[d+"_"+servico]){
                                return groupedByDimAndServico[d+"_"+servico][0].count;
                            }

                            return 0;
                        });

                        return _.concat([d], values);
                    });
                    
                    $scope.Chart2 = c3.generate({
                        bindto: '#chart2',               
                        data: {
                            type: "bar",
                            columns: data,
                            colors: {
                                "Péssimo": "#0D0D0D",
                                "Ruim": "#FF0000",
                                "Regular": "#FFA500",
                                "Bom": "#008000",
                                "Ótimo": "#6495ED"
                            }
                        },
                        padding: {
                            bottom: 20
                            },
                        groups: [dim],
                        axis: {
                            x: {
                                tick: {
                                    format: function (x) {return servicos[x] }
                                }
                            }
                        }
                    }); 
                }                    

				var initController = function(){
                    Submissions.count({formId: $stateParams.formId}, function(submissions){
                        $scope.ResponseCount = submissions.toJSON().count;
                    });

                    ResponsesByServico.get({formId: $stateParams.formId}, function(response){
                        $scope.qualidadeServicoChartData = response;
                        $scope.generateQualidadeServicoChart(response);
                    } );

                    Stats.get({formId: $stateParams.formId}, function(data){

                        var groupedByDate = _.groupBy(data, "_id.date");
                        var x = _.keys(groupedByDate);

                        var series = {
                            "login": [],
                            "saved": [],
                            "submitted": []
                        }
                        
                        _.forEach(groupedByDate, function(v, k){
                            var byType = _.groupBy(v, "_id.type");

                            byType = _.merge(
                                {
                                    "login":  [{"count": 0}],
                                    "saved":  [{"count": 0}],
                                    "submitted":  [{"count": 0}]
                                },
                                byType
                            )

                            _.forEach(byType, function(count, type){
                                series[type].push(count[0].count);
                            });
                            
                        });

                        $scope.LoginCount = _.sum(series["login"]);

                        $scope.Chart.load({
                            columns: [
                                _.concat(['x'], x),
                                _.concat(['Logins'], series["login"]),
                                _.concat(['Salvos'], series["saved"]),
                                _.concat(['Finalizados'], series["submitted"]),
                            ]
                        })
                    })
                    

					Forms.get({
						formId: $stateParams.formId
					}, function(form){
                        Submissions.query({formId: $stateParams.formId}, function(submissions){
                            $scope.myform = form;
                            var defaultFormFields = _.cloneDeep($scope.myform.form_fields);

                            //Iterate through form's submissions
                            for(var i = 0; i < submissions.length; i++){
                                for(var x = 0; x < submissions[i].form_fields; x++){
                                    var oldValue = submissions[i].form_fields[x].fieldValue || '';
                                    submissions[i].form_fields[x] =  _.merge(defaultFormFields, submissions[i].form_fields);
                                    submissions[i].form_fields[x].fieldValue = oldValue;
                                }
                                submissions[i].selected = false;
                            }

                            $scope.table.rows = submissions;


                            /*
                            ** Analytics Functions
                            */
                            $scope.AverageTimeElapsed = (function(){
                                var totalTime = 0;
                                var numSubmissions = $scope.table.rows.length;
                                
                                for(var i=0; i<$scope.table.rows.length; i++){
                                    totalTime += $scope.table.rows[i].timeElapsed;
                                }

                                return totalTime/numSubmissions;
                            })();

                        });
                    });
				};
				initController();

				var updateFields = $interval(initController, 10000);

				$scope.$on("$destroy", function() {
					if (updateFields) {
						$interval.cancel($scope.updateFields);
					}
				});

                /*
                ** Table Functions
                */
                $scope.isAtLeastOneChecked = function(){
                    for(var i=0; i<$scope.table.rows.length; i++){
                        if($scope.table.rows[i].selected) return true;
                    }
                    return false;
                };
                $scope.toggleAllCheckers = function(){
                    for(var i=0; i<$scope.table.rows.length; i++){
                        $scope.table.rows[i].selected = $scope.table.masterChecker;
                    }
                };
                $scope.toggleObjSelection = function($event, description) {
                    $event.stopPropagation();
                };
                $scope.rowClicked = function(row_index) {
                   $scope.table.rows[row_index].selected = !$scope.table.rows[row_index].selected;
                };

                /*
                * Form Submission Methods
                */

                //Fetch and display submissions of Form

                //Delete selected submissions of Form
                $scope.deleteSelectedSubmissions = function(){

                    var delete_ids = _.chain($scope.table.rows).filter(function(row){
                        return !!row.selected;
                    }).pluck('_id').value();

                    $http({ url: '/forms/'+$scope.myform._id+'/submissions',
                            method: 'DELETE',
                            data: {deleted_submissions: delete_ids},
                            headers: {'Content-Type': 'application/json;charset=utf-8'}
                        }).success(function(data, status, headers){
                            //Remove deleted ids from table
                            var tmpArray = [];
                            for(var i=0; i<$scope.table.rows.length; i++){
                                if(!$scope.table.rows[i].selected){
                                    tmpArray.push($scope.table.rows[i]);
                                }
                            }
                            $scope.table.rows = tmpArray;
                        })
                        .error(function(err){
                            console.log('Could not delete form submissions.\nError: ');
                            console.log(err);
                            console.error = err;
                        });
                };

                //Export selected submissions of Form
                $scope.exportSubmissions = function(type){
                    var fileMIMETypeMap = {
                        'xls': 'vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'json': 'json',
                        'csv': 'csv'
                    };

					angular.element('#table-submission-data').tableExport({type: type, escape:false});
                };

            }
        };
    }
]);
