'use strict';

//Submissions service used for communicating with the forms REST endpoints
angular.module('forms').factory('Submissions', ['$resource',
	function($resource) {
		return $resource('/forms/:formId/submissions', {
			formId: '@_id'
		}, {
			'query' : {
				method: 'GET', 
				isArray: true
			},
			'count' : {
				method: 'GET',
				url: '/forms/:formId/submissions/count'
			},
			'update': {
				method: 'PUT'
			},
			'save': {
				method: 'POST'
			}
		});
	}
]);

//Submissions service used for communicating with the forms REST endpoints
angular.module('forms').factory('Stats', ['$resource',
	function($resource) {
		return $resource('/forms/:formId/stats', {
			formId: '@_id'
		}, {
			'get' : {
				method: 'GET', 
				isArray: true
			},
		});
	}
]);


angular.module('forms').factory('ResponsesByServico', ['$resource',
function($resource) {
	return $resource('/forms/:formId/responsesByServico', {
		formId: '@_id'
	}, {
		'get' : {
			method: 'GET', 
			isArray: true
		},
	});
}
]);