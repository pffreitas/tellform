'use strict';

// Forms controller
angular.module('forms').controller('AdminFormController', ['$rootScope', '$scope', '$stateParams', '$state', 'Forms', 'CurrentForm', '$http', '$uibModal', 'myForm', '$filter', '$sce',
	function($rootScope, $scope, $stateParams, $state, Forms, CurrentForm, $http, $uibModal, myForm, $filter, $sce) {

		$scope.trustSrc = function(src) {
			return $sce.trustAsResourceUrl(src);
		};

        $scope.loadRespondents = function(){
            $http.get('/respondent/' + $scope.myform.id)
            .success(function(data){
                $scope.respondents = data.respondents;
            });
        }

		//Set active tab to Create
		$scope.activePill = 0;

        $scope = $rootScope;
        $scope.animationsEnabled = true;
        $scope.myform = myForm;
        $scope.originalForm = myForm;
        $rootScope.saveInProgress = false;

        CurrentForm.setForm($scope.myform);

		$scope.formURL = "/#!/forms/" + $scope.myform._id;

		$scope.actualFormURL = $scope.formURL;

		var refreshFrame = $scope.refreshFrame = function(){
			if(document.getElementById('iframe')) {
				document.getElementById('iframe').contentWindow.location.reload();
			}
		};

		$scope.tabData   = [
            {
                heading: $filter('translate')('ANALYZE_TAB'),
				templateName:   'analyze'
            }
        ];


        $scope.setForm = function(form){
            $scope.myform = form;
        };
		
        $rootScope.resetForm = function(){
            $scope.myform = Forms.get({
                formId: $stateParams.formId
            });
        };

        $scope.generating = false;
        $scope.genRespondentCredentials = function(q){
            $scope.generating = true;
            $http.post('/respondent/gen/' + $rootScope.myform.id, {qtd: q})
            .success(function(data){
                $scope.generating = false;
                $rootScope.respondents = data.respondents
            });
        }        

	}
]);
