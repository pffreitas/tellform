'use strict';

angular.module('respondent').controller('RespondentController', ['$scope', '$location', '$state', '$rootScope', 'Respondents',
	function($scope, $location, $state, $rootScope, Respondents) {
    
        $scope.findAll = function() {
            Respondents.query(function(_resps){
                $scope.respondents = _resps;
            });
        };

    }
]);