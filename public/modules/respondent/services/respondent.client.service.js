
angular.module('respondent').factory('Respondents', ['$resource',
	function($resource) {
		return $resource('/respondents', {
			respId: '@_id'
		}, {
			'query' : {
				method: 'GET',
				isArray: true
			}
		});
	}
]);