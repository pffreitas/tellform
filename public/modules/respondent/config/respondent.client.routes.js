'use strict';

// Setting up route
angular.module('respondent').config(['$stateProvider',
	function($stateProvider) {

	// Users state routing
	$stateProvider.
		state('list', {
			url: '/respondent/list',
			templateUrl: 'modules/respondent/views/list.client.view.html'
		});
	}
]);
