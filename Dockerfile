FROM  node:7

# 4545 = TellForm server
EXPOSE 4545

# Set development environment as default
ENV NODE_ENV production
ENV PORT 4545
ENV BASE_URL http://www.perfixforms.com.br

RUN npm install --quiet -g grunt bower pm2@2.9.2

RUN mkdir -p /opt/tellform/public/lib
WORKDIR /opt/tellform

COPY package.json /opt/tellform/package.json

COPY bower.json /opt/tellform/bower.json
COPY .bowerrc /opt/tellform/.bowerrc

COPY ./app /opt/tellform/app
COPY ./public /opt/tellform/public
COPY ./config /opt/tellform/config
COPY ./gruntfile.js /opt/tellform/gruntfile.js
COPY ./server.js /opt/tellform/server.js
COPY ./process.yml /opt/tellform/process.yml
COPY ./scripts /opt/tellform/scripts

RUN npm install && bower install && grunt build

# Run TellForm server
CMD ["pm2-docker","process.yml"]
