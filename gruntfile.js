'use strict';
var spawn = require('child_process').spawn;


module.exports = function(grunt) {
	require('jit-grunt')(grunt);

	// Project Configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		clean: {
  			build: ['public/dist']
		},
		concat: {
			options: {
				separator: '\n ; //sep \n'
			},
			vendor: {
				src: '<%= bowerJavaScriptFiles %>',
				dest: './public/dist/app/vendor.js'
			},
			app: {
				src: '<%= applicationJavaScriptFiles %>',
				dest: './public/dist/app/<%= pkg.name %>.js'
			}
		},
		cssmin: {
			combine: {
				files: {
					'public/dist/application.min.css': '<%= applicationCSSFiles %>'
				}
			}
		},
		nodemon: {
			dev: {
				script: 'server.js',
				options: {
					ext: 'js,html'
				}
			}
		},
		concurrent: {
		    default: ['nodemon'],
	    	options: {
				logConcurrentOutput: true,
		    	limit: 10
	    	}
	    },
		env: {
			production: {
				NODE_ENV: 'production',
				src: '.env'
			},
			dev: {
				NODE_ENV: 'development',
				src: '.env'
			}
		},
		html2js: {
			options: {
				base: 'public',
				watch: true,
				module: 'NodeForm.templates',
				singleModule: true,
				useStrict: true,
				htmlmin: {
					collapseBooleanAttributes: true,
					collapseWhitespace: true,
					removeAttributeQuotes: true,
					removeComments: true,
					removeEmptyAttributes: true,
					removeRedundantAttributes: true
				}
			},
			main: {
				src: ['public/modules/**/views/**.html', 'public/modules/**/views/**/*.html'],
				dest: 'public/dist/populate_template_cache.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-concat');

	// Making grunt default to force in order not to break the project.
	grunt.option('force', true);

	// A Task for loading the configuration object
	grunt.task.registerTask('loadConfig', 'Task that loads the config into a grunt option.', function() {
		var init = require('./config/init')();
		var config = require('./config/config');

		console.log(config.getBowerJavascriptFiles());
		console.log(config.assets.js);

		grunt.config.set('bowerJavaScriptFiles', config.getBowerJavascriptFiles());
		grunt.config.set('applicationJavaScriptFiles', config.assets.js);
		grunt.config.set('applicationCSSFiles', config.assets.css);
	});

	// Default task(s).
	grunt.registerTask('default', ['clean', 'loadConfig', 'html2js:main', 'cssmin', 'concat', 'concurrent:default']);

	// Build task(s).
	grunt.registerTask('build', ['clean', 'loadConfig', 'html2js:main', 'cssmin', 'concat']);

};
