'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	forms = require('../../app/controllers/forms.server.controller'),
	config = require('../../config/config'),
	auth = require('../../config/passport_helpers');


module.exports = function(app) {
	// Form Routes
	app.route('/forms')
		.get(auth.isAuthenticatedOrApiKey, forms.list)
		.post(auth.isAuthenticatedOrApiKey, forms.create);

	app.route('/forms/generate')
		.get(auth.isAuthenticatedOrApiKey, forms.generate);		

	app.get('/bndes', function(req, res){
		res.redirect('/#!/forms/5946fcee4766401100981163');
	});

	app.route('/forms/:formId([a-zA-Z0-9]+)')
		.get(forms.read)
		.post(forms.createSubmission)
		.put(auth.isAuthenticatedOrApiKey, forms.hasAuthorization, forms.update)
		.delete(auth.isAuthenticatedOrApiKey, forms.hasAuthorization, forms.delete);

	app.route('/forms/:formId([a-zA-Z0-9]+)/submissions')
		.get(auth.isAuthenticatedOrApiKey, forms.hasAuthorization, forms.listSubmissions)
		.delete(auth.isAuthenticatedOrApiKey, forms.hasAuthorization, forms.deleteSubmissions);

	app.route('/forms/:formId([a-zA-Z0-9]+)/submissions/count')
		.get(auth.isAuthenticatedOrApiKey, forms.hasAuthorization, forms.countSubmissions)

	app.route('/forms/:formId([a-zA-Z0-9]+)/stats')
		.get(auth.isAuthenticatedOrApiKey, forms.hasAuthorization, forms.stats)

		app.route('/forms/:formId([a-zA-Z0-9]+)/responsesByServico')
		.get(auth.isAuthenticatedOrApiKey, forms.hasAuthorization, forms.responseByServico)

	app.param('formId', forms.formByID);
};
