'use strict';

/**
 * Module dependencies.
 */
var respondent = require('../../app/controllers/respondent.server.controller'),
    forms = require('../../app/controllers/forms.server.controller');

module.exports = function(app) {

	app.route('/respondent/gen/:formId([a-zA-Z0-9]+)')
		.post(respondent.gen);

	app.route('/respondent/gen/:formId([a-zA-Z0-9]+)')
		.get(respondent.genCsv);


	app.route('/respondent/load/:formId([a-zA-Z0-9]+)')
		.get(respondent.loadRespondents);		
		

	app.route('/respondent/auth/:formId([a-zA-Z0-9]+)')
		.post(respondent.auth);		

	app.route('/respondent/:formId([a-zA-Z0-9]+)')
		.get(respondent.findAll);

};