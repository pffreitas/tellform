'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	mongoose = require('mongoose'),
    crypto = require('crypto'),
    _ = require('lodash'),
    FormSubmission = mongoose.model('FormSubmission'),
    Respondent = mongoose.model('Respondent'),
    FormStats = mongoose.model('FormStats'),
    axios = require('axios'),
    json2csv = require('json2csv'),
    querystring = require('querystring'),
    fs = require('fs'),
	csv = require('fast-csv'),
	_ = require('lodash');


function mapRespondent(r){
    return {
        id: r._id,
        username: r.username,
        passwordText: r.passwordText,
        hasResponded: r.hasResponded,
        started: r.started
    }
}

exports.auth = function(req, res){
    var form = req.form;

    Respondent.findOne({formId: form._id, username: req.body.code},
        function (err, respondent) {
            if(!respondent){
                res.status(400).send({
                    msg: "Credenciais de acesso inválidas."
                });
            } else {
                if(respondent.hasResponded === true){
                    res.status(400).send({
                        msg: "A pesquisa já foi respondida com este login/senha."
                    });
                } else {
                    var hashed = respondent.hashPassword(req.body.password);
                    if(hashed !== respondent.password){
                        res.status(400).send({msg: "Credenciais de acesso inválidas."})
                    } else {
                        if(req.body.captcha){
                            var payload = {
                                secret: "6Le2dRoUAAAAAPQEfWzzhyMJnwK6wB7BItjdJjs_",
                                response: req.body.captcha
                            };

                            axios
                                .post("https://www.google.com/recaptcha/api/siteverify", querystring.stringify(payload))
                                .then(function(response){
                                    if(response.data && response.data.success) {
                                        Respondent.findByIdAndUpdate(respondent._id, {started: true}, 
                                            function (err, resp){});
                                        new FormStats({form: form, type: 'login'}).save();

                                        FormSubmission.findOne({respondentId: respondent._id}, function(err, sub){
                                            res.json({
                                                formId: form._id,
                                                respondent: respondent,
                                                submission: sub
                                            });  
                                        });                       
                                    } else {
                                        res.status(400).send({msg: "CAPTCHA Inválido"});
                                    }         
                                })
                                .catch(function(error){
                                    res.status(400).send({msg: "CAPTCHA Inválido"});
                                }); 
                        }else{
                            Respondent.findByIdAndUpdate(respondent._id, {started: true}, 
                                            function (err, resp){});
                            new FormStats({form: form, type: 'login'}).save();

                            FormSubmission.findOne({respondentId: respondent._id}, function(err, sub){
                                res.json({
                                    formId: form._id,
                                    respondent: respondent,
                                    submission: sub
                                });  
                            });  
                        }
                    }
                }
            }
        }
    ); 
}


exports.loadRespondents = function(req, res) {
    var form = req.form;
    var respondentBag = [];
    var stream = fs.createReadStream("scripts/respondents.csv");
    
    var count = 0;
	var csvStream = csv({quote: '"'})
		.on("data", function(data){
            count++;

			var username = data[0];
			var password = data[1];

            console.log(count, username, password);
            
            respondentBag.push(new Respondent({
                username: username,
                password: password,
                passwordText: password,
                formId: form._id
            }));
		})
		.on("end", function(){
            console.log("END>>>>", respondentBag.length);
			Respondent.insertMany(respondentBag).then(function(){
                res.status(200).json({"ok": true})
            });		
		});
	
	stream.pipe(csvStream);
}  

exports.genCsv = function(req, res) {
    var form = req.form;
    var qtd = req.query.qtd;

    console.log("gerando ", qtd, " acessos");

    var respondentBag = _.range(qtd).map(function() {
        var username = crypto.randomBytes(3).toString('hex');
        var password = crypto.randomBytes(5).toString('hex');

        return new Respondent({
            username: username,
            password: password,
            passwordText: password,
            formId: form._id
        });
    });

    var onSave = function (docs) {
        Respondent.find({formId: form._id},
            function (err, respondents) {
                var fields = ['username', 'passwordText'];
                var fieldNames = ['Username', 'Password'];
                var data = json2csv({ data: respondents, fields: fields, fieldNames: fieldNames });

                res.attachment('acessos.csv');
                res.status(200).send(data);
            }
        );          
    }

    Respondent.insertMany(respondentBag).then(onSave);
}  

exports.gen = function(req, res) {
    var form = req.form;
    var qtd = req.body.qtd;

    var respondentBag = [];
    var arr = [];
    var i = 0;
    while(i < qtd){
        var username = String(Math.floor(Math.random() * 90000) + 10000);
        if(arr.indexOf(username) > -1) continue;
        arr.push(username);

        var password = String(Math.floor(Math.random() * 90000) + 10000);
        console.log(i, username, password);

        respondentBag.push(new Respondent({
            username: username,
            password: password,
            passwordText: password,
            formId: form._id
        }));

        i++;
    }

    var onSave = function (docs) {
        Respondent.find({formId: form._id},
            function (err, respondents) {
                res.json({
                    formId: form._id,
                    respondents: _.map(respondents, mapRespondent)
                });
            }
        );          
    }

    Respondent.insertMany(respondentBag).then(onSave).catch(function(e){
        console.log(e);
    });
}   


exports.findAll = function(req, res) {
    var form = req.form;

    Respondent.find(
        {formId: form._id},
        function (err, respondents) {
            res.json({
                formId: form._id,
                respondents: _.map(respondents, mapRespondent)
            });
        }
    );   
}; 

