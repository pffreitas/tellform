"use strict";

/**
 * Module dependencies.
 */
var mongoose = require("mongoose"),
  errorHandler = require("./errors.server.controller"),
  Form = mongoose.model("Form"),
  Field = mongoose.model("Field"),
  FormSubmission = mongoose.model("FormSubmission"),
  Respondent = mongoose.model("Respondent"),
  FormStats = mongoose.model("FormStats"),
  User = mongoose.model("User"),
  config = require("../../config/config"),
  diff = require("deep-diff"),
  fs = require("fs"),
  csv = require("fast-csv"),
  _ = require("lodash");

/**
 * Delete a forms submissions
 */
exports.deleteSubmissions = function(req, res) {
  var submission_id_list = req.body.deleted_submissions,
    form = req.form;

  FormSubmission.remove(
    { form: req.form, admin: req.user, _id: { $in: submission_id_list } },
    function(err) {
      if (err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
        return;
      }

      form.analytics.visitors = [];
      form.save(function(err) {
        if (err) {
          res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
          return;
        }
        res.status(200).send("Form submissions successfully deleted");
      });
    }
  );
};

/**
 * Submit a form entry
 */
exports.createSubmission = function(req, res) {
  var form = req.form;

  if (!req.body.partial) {
    Respondent.findByIdAndUpdate(
      req.body.respondentId,
      { hasResponded: true },
      function(err, resp) {
        //handle error?
      }
    );
  }

  var save = function(doc) {
    var submission = _.extend(doc, {
      admin: req.form.admin._id,
      form: req.form._id,
      title: req.form.title,
      partial: req.body.partial,
      respondentId: req.body.respondentId,
      form_fields: req.body.form_fields,
      timeElapsed: req.body.timeElapsed,
      percentageComplete: req.body.percentageComplete,
      servico: req.body.form_fields[0].fieldValue["servico"]
    });

    if (req.device) {
      submission.device = req.device;
    }

    submission.save(function(err, submission) {
      if (err) {
        console.error(err.message);
        return res.status(500).send({
          message: errorHandler.getErrorMessage(err)
        });
      }

      if (req.body.partial) {
        new FormStats({ form: form, type: "saved" }).save();
      } else {
        new FormStats({ form: form, type: "submitted" }).save();
      }

      res.status(200).send("Form submission successfully saved");
    });
  };

  FormSubmission.findById(req.body.prevSubmission, function(err, doc) {
    var doc = doc ? doc : new FormSubmission();
    save(doc);
  });
};

exports.responseByServico = function(req, res) {
  var aggregate = [
    { $project: { _id: 0, form_fields: 1, servico: 1 } },
    { $unwind: "$form_fields" },
    {
      $group: {
        _id: { field: "$form_fields", servico: "$servico" },
        count: { $sum: 1 }
      }
    },
    {
      $project: {
        _id: 0,
        value: "$_id.field.fieldValue",
        idx: "$_id.field.index",
        servico: "$_id.servico",
        count: { $sum: 1 }
      }
    },
    { $match: { idx: { $gt: 0 } } },
    {
      $group: {
        _id: { value: "$value", servico: "$servico" },
        count: { $sum: 1 }
      }
    }
  ];

  FormSubmission.aggregate(aggregate).exec(function(err, data) {
    res.json(data);
  });
};

exports.stats = function(req, res) {
  var aggregate = [
    { $match: { form: req.form._id } },
    {
      $project: {
        date: {
          $dateToString: {
            format: "%Y-%m-%d",
            date: "$createdAt"
          }
        },
        type: true
      }
    },
    {
      $group: {
        _id: {
          date: "$date",
          type: "$type"
        },
        count: {
          $sum: 1
        }
      }
    }
  ];

  FormStats.aggregate(aggregate).exec(function(err, data) {
    res.json(data);
  });
};

/**
 * Get List of Submissions for a given Form
 */
exports.listSubmissions = function(req, res) {
  var _form = req.form;
  var _user = req.user;

  FormSubmission.find({ form: _form._id }).exec(function(err, _submissions) {
    if (err) {
      console.error(err);
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    res.json(_submissions);
  });
};

exports.countSubmissions = function(req, res) {
  var _form = req.form;

  FormSubmission.count({ form: _form._id, partial: false }).exec(function(
    err,
    _submissions
  ) {
    if (err) {
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    res.json({ count: _submissions });
  });
};

/**
 * Create a new form
 */
exports.create = function(req, res) {
  if (!req.body.form) {
    return res.status(400).send({
      message: "Invalid Input"
    });
  }
  var form = new Form(req.body.form);

  form.admin = req.user._id;

  form.save(function(err) {
    if (err) {
      console.log(err);
      return res.status(405).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    res.json(form);
  });
};

var createOptionsDes = function() {
  return [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Totalmente Insatisfeito"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Parcialmente insatisfeito"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Indiferente"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Parcialmente Satisfeito"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Totalmente satisfeito"
    }
  ];
};

var createOptionsCheckbox = function() {
  return [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Iluminação"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Ruído"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Limpeza do ambiente de trabalho"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Ventilação e temperatura"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Equipamento de informática"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Espaço de trabalho"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Móveis e equipamentos"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Outra"
    }
  ];
};

var createStatementInsatisfeito = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldType = "statement";
  field.fieldOptions = createOptionsDes();
  return field;
};

var createRegiao = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldType = "radio";
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Norte"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Nordeste"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Sul"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Sudeste"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Centro-Oeste"
    }
  ];
  return field;
};

var createFaixaEtaria = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldType = "radio";
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "21 a 37"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "38 a 44"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "45 a 51"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "52 a 77"
    }
  ];
  return field;
};

var createGenero = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldType = "radio";
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Masculino"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Feminino"
    }
  ];
  return field;
};

var createEscolaridade = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldType = "radio";
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Doutorado"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Mestrado"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Pós-Graduado"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Ensino Superior"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Ensino Médio"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Ensino Fundamental"
    }
  ];
  return field;
};

var createLotacao = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldType = "dropdown";
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SEDE - CENTRO CORPORATIVO"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "SBBR - AEROPORTO INTERNACIONAL DE BRASÍLIA - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBBE - AEROPORTO INTERNACIONAL DE BELÉM - GRUPO I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBBH - AEROPORTO DE BELO HORIZONTE - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBBV - AEROPORTO INTERNACIONAL DE BOA VISTA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "SBCT - AEROPORTO INTERNACIONAL DE CURITIBA - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBFL - AEROPORTO INTERNACIONAL DE FLORIANÓPOLIS - GRUPO I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBFI - AEROPORTO INTERNACIONAL DE FOZ DO IGUAÇU - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "SBFZ - AEROPORTO INTERNACIONAL DE FORTALEZA - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBGO - AEROPORTO DE GOIANIA - GRUPO I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBJV - AEROPORTO DE JOINVILLE - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "SBPA - AEROPORTO INTERNACIONAL DE PORTO ALEGRE - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBRF - AEROPORTO INTERNACIONAL DE RECIFE - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "SBSV - AEROPORTO INTERNACIONAL DE SALVADOR - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBAR - AEROPORTO DE ARACAJU - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBCG - AEROPORTO INTERNACIONAL DE CAMPO GRANDE - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBCR - AEROPORTO INTERNACIONAL DE CORUMBÁ - GRUPO IV"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBCY - AEROPORTO INTERNACIONAL DE CUIABÁ - GRUPO I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBMO - AEROPORTO INTERNACIONAL DE MACEIÓ - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBSL - AEROPORTO INTERNACIONAL DE SÃO LUÍS - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBTE - AEROPORTO DE TERESINA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBVT - AEROPORTO DE VITÓRIA - GRUPO I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBSP - AEROPORTO DE SÃO PAULO/CONGONHAS - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBEG - AEROPORTO INTERNACIONAL DE MANAUS - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "SBKP - AEROPORTO INTERNACIONAL DE CAMPINAS - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBPR - AEROPORTO DE CARLOS PRATES - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBNT - AEROPORTO INTERNACIONAL DE NATAL - GRUPO I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBRB - AEROPORTO DE RIO BRANCO - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBPV - AEROPORTO INTERNACIONAL DE PORTO VELHO - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBMQ - AEROPORTO INTERNACIONAL DE MACAPÁ - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBJP - AEROPORTO INTERNACIONAL DE JOÃO PESSOA - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBMT - AEROPORTO CAMPO DE MARTE - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBNF - AEROPORTO INTERNACIONAL DE NAVEGANTES - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBLO - AEROPORTO DE LONDRINA - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBBI - AEROPORTO DE BACACHERI - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBUR - AEROPORTO DE UBERABA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBUL - AEROPORTO DE UBERLÂNDIA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBMK - AEROPORTO DE MONTES CLAROS - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBSN - AEROPORTO DE SANTARÉM - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBHT - AEROPORTO DE ALTAMIRA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "SBCZ - AEROPORTO INTERNACIONAL DE CRUZEIRO DO SUL/AC-MARMUD CAMELI"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBTT - AEROPORTO INTERNACIONAL DE TABATINGA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBTF - AEROPORTO DE TEFÉ - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBJC - AEROPORTO DE BELÉM - GRUPO IV"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBIZ - AEROPORTO DE IMPERATRIZ - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBMA - AEROPORTO DE MARABÁ - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBPP - AEROPORTO INTERNACIONAL DE PONTA PORÃ - GRUPO IV"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBKG - AEROPORTO DE CAMPINA GRANDE - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBUF - AEROPORTO DE PAULO AFONSO - GRUPO IV"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBBG - AEROPORTO INTERNACIONAL DE BAGÉ - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBUG - AEROPORTO INTERNACIONAL DE URUGUAIANA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBPK - AEROPORTO INTERNACIONAL DE PELOTAS - GRUPO IV"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBIL - AEROPORTO DE ILHÉUS - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBPL - AEROPORTO DE PETROLINA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBGR - AEROPORTO INTERNACIONAL GUARULHOS - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBCF - AEROPORTO INTERNACIONAL DE CONFINS - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBCJ - AEROPORTO DE CARAJÁS - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBGL - AEROPORTO INTERNACIONAL DO GALEÃO - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBRJ - AEROPORTO SANTOS DUMONT - GRUPO ESPECIAL"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBCP - AEROPORTO DE CAMPOS DOS GOYTACAZES - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBME - AEROPORTO DE MACAÉ - GRUPO II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBJR - AEROPORTO DE JACAREPAGUÁ - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TABU - EPTA BAURU - CATEGORIA  ESP-III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TARP - EPTA RIBEIRÃO PRETO - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAAT - EPTA ALTA FLORESTA - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TADN - EPTA PRESIDENTE PRUDENTE - CATEGORIA ESP-III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAIH - EPTA ITAITUBA - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAAA - EPTA CONCEIÇÃO DO ARAGUAIA - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TABW - EPTA BARRA DO GARÇAS - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TACI - EPTA CAROLINA - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TACV - EPTA CARAVELAS - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAEK - EPTA JACAREACANGA - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAIC - EPTA ITACOATIARA - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TALP - EPTA BOM JESUS DA LAPA - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAMD - EPTA MONTE DOURADO - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAMS - EPTA MOSSORÓ - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAMY - EPTA MANICORÉ - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBPB - AEROPORTO DE PARNAÍBA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAPC - EPTA POÇOS DE CALDAS - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TATK - EPTA TARAUACÁ - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TATU - EPTA TUCURUÍ - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBJU - AEROPORTO DE JUAZEIRO DO NORTE - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBCM - AEROPORTO DE CRICIÚMA/FORQUILHINHA - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBSJ - AEROPORTO DE SÃO JOSÉ DOS CAMPOS - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "SBPJ - AEROPORTO DE PALMAS - GRUPO III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "CSAT - CENTRO DE SERVIÇOS ADMINISTRATIVOS E TÉCNICOS"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "UASP - UNIDADE DE APOIO À EXECUÇÃO DE SERVIÇOS DE SÃO PAULO"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "UARJ - UNIDADE DE APOIO À EXECUÇÃO DE SERVIÇOS DO RIO DE JANEIRO"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "UAMN - UNIDADE DE APOIO À EXECUÇÃO DE SERVIÇOS DE MANAUS"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "UARF - UNIDADE DE APOIO À EXECUÇÃO DE SERVIÇOS DE RECIFE"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TATE - EPTA TERESINA - CATEGORIA ESP- III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAPL - EPTA PETROLINA - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAJP - EPTA JOÃO PESSOA - CATEGORIA ESP-III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAKG - EPTA CAMPINA GRANDE - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAGR - EPTA SÃO PAULO/GUARULHOS - CATEGORIA ESP-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAKP - EPTA VIRACOPOS/CAMPINAS - CATEGORIA ESP-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAJV - EPTA JOINVILLE - CATEGORIA ESP-III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAPB - EPTA PARNAÍBA - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TABG - EPTA BAGÉ - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAPK - EPTA PELOTAS - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAPP - EPTA PONTA PORÃ - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAGO - EPTA GOIÂNIA - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TALO - EPTA LONDRINA - CATEGORIA ESP- II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAMQ - EPTA MACAPÁ - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TATF - EPTA TEFÉ - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TACJ - EPTA CARAJÁS - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAMA - EPTA MARABÁ - CATEGORIA ESP-III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAJC - EPTA BELÉM - CATEGORIA ESP-III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TASN - EPTA SANTARÉM - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAHT - EPTA ALTAMIRA - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAJU - EPTA JUAZEIRO DO NORTE - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TACZ - EPTA CRUZEIRO DO SUL - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAPJ - EPTA PALMAS - CATEGORIA ESP- III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TABH - EPTA BELO HORIZONTE/PAMPULHA - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAPR - EPTA BELO HORIZONTE/CARLOS PRATES - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAMK - EPTA MONTES CLAROS - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAUL - EPTA UBERLÂNDIA - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAUR - EPTA UBERABA - CATEGORIA ESP-III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAJR - EPTA JACAREPAGUÁ - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAME - EPTA MACAÉ - CATEGORIA ESP-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TACP - EPTA CAMPOS - CATEGORIA  A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TARJ - EPTA RIO DE JANEIRO/SANTOS DUMONT - CATEGORIA ESP-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAGL - EPTA RIO DE JANEIRO/GALEÃO - CATEGORIA ESP-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAVT - EPTA VITÓRIA - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TANF - EPTA NAVEGANTES - CATEGORIA ESP-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAIL - EPTA ILHÉUS - CATEGORIA ESP-III"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAUF - EPTA PAULO AFONSO - CATEGORIA A-II"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "TAIZ - EPTA IMPERATRIZ - CATEGORIA A-I"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value:
        "SBPO - AEROPORTO REGIONAL DO PLANALTO SERRANO - CORREIA PINTO"
    }
  ];
  return field;
};

var createStatementConcordo = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldType = "statement";
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Discordo Totalmente"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Discordo"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Nem concordo, nem discordo"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Concordo"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Concordo Totalmente"
    }
  ];
  return field;
};

var create2Ans = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldOptions = createOptionsDes();
  field.fieldType = "twoa_radio";
  return field;
};

var createCheckbox = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldOptions = createOptionsCheckbox();
  field.fieldType = "checkbox";
  return field;
};

var createSingleAns = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldOptions = createOptionsDes();
  field.fieldType = "radio";
  return field;
};

var createConcordo = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Discordo Totalmente"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Discordo"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Nem concordo, nem discordo"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Concordo"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Concordo Totalmente"
    }
  ];
  field.fieldType = "radio";
  return field;
};

var createYesNo = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Sim"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Não"
    }
  ];
  field.fieldType = "radio";
  return field;
};

var createInternacao = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Clínica"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "Maternidade"
    }
  ];
  field.fieldType = "radio";
  return field;
};

var createTempo = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldOptions = [
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "30m ou menos"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "1h"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "2h"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "3h"
    },
    {
      option_id: Math.floor(100000 * Math.random()),
      option_title: "",
      option_value: "4h ou mais"
    }
  ];
  field.fieldType = "radio";
  return field;
};

var createParagraph = function(titulo, descr) {
  var field = new Field();
  field.title = titulo;
  field.description = descr;
  field.required = false;
  field.fieldType = "textarea";
  return field;
};

var createDadosPessoais = function() {
  var field = new Field();
  field.title = "Informações Demográficas";
  field.description = "Informações Demográficas";
  field.required = false;
  field.fieldType = "resp_info_2";
  return field;
};

exports.generate = function(req, res) {
  User.findOne({ username: "pfreitas" }, function(err, user) {
    var form = new Form();
    form.admin = user._id;
    form.title = req.query.title;
    form.startPage.introButtonText = "Iniciar";
    form.startPage.introTitle = "Pesquisa de Clima FINDES 2019";
    form.startPage.introParagraph =
      "<p>Prezado colaborador, é com grande satisfação que disponibilizamos nossa pesquisa de clima, sua contribuição será de grande valor ao processo.</p><p>Seus dados pessoais estarão protegidos e suas respostas terão sigilo absoluto.</p><p>Para cada afirmação, você deve dar sua opinião pessoal e sincera, avaliando sua área e o sistema FINDES como um todo.</p>";
    form.startPage.showStart = true;
    form.isLive = true;
    form.form_fields = [];

    if (req.query.dadosPessoais == 1) {
      form.form_fields.push(createDadosPessoais());
    }

    var stream = fs.createReadStream(
      "scripts/perguntas_" + req.query.instrumento + ".csv"
    );
    var csvStream = csv({ quote: '"' })
      .on("data", function(data) {
        var descr = data[0];
        var titulo = data[1];
        var type = data[2];

        console.log(data);

        if (type === "Two Answers") {
          form.form_fields.push(create2Ans(titulo, descr));
        }

        if (type === "Só BNDES") {
          form.form_fields.push(createSingleAns(titulo, descr));
        }

        if (type === "Paragraph") {
          form.form_fields.push(createParagraph(titulo, descr));
        }

        if (type === "Yes/No") {
          form.form_fields.push(createYesNo(titulo, descr));
        }

        if (type === "checkbox") {
          form.form_fields.push(createCheckbox(titulo, descr));
        }

        if (type === "Internacao") {
          form.form_fields.push(createInternacao(titulo, descr));
        }

        if (type === "Tempo") {
          form.form_fields.push(createTempo(titulo, descr));
        }

        if (type === "Concordo") {
          form.form_fields.push(createConcordo(titulo, descr));
        }

        if (type === "Statement") {
          form.form_fields.push(createStatementConcordo(titulo, descr));
        }

        if (type === "Statement Insatisfeito") {
          form.form_fields.push(createStatementInsatisfeito(titulo, descr));
        }

        if (type === "Regiao") {
          form.form_fields.push(createRegiao(titulo, descr));
        }

        if (type === "Faixa Etaria") {
          form.form_fields.push(createFaixaEtaria(titulo, descr));
        }

        if (type === "Genero") {
          form.form_fields.push(createGenero(titulo, descr));
        }

        if (type === "Escolaridade") {
          form.form_fields.push(createEscolaridade(titulo, descr));
        }

        if (type === "lotacao") {
          form.form_fields.push(createLotacao(titulo, descr));
        }
      })
      .on("end", function() {
        form.save(function(err) {
          if (err) {
            console.log(err);
            return res.status(405).send({
              message: errorHandler.getErrorMessage(err)
            });
          }

          res.json(form);
        });
      });

    stream.pipe(csvStream);
  });
};

/**
 * Show the current form
 */
exports.read = function(req, res) {
  FormSubmission.find({ form: req.form._id }).exec(function(err, _submissions) {
    if (err) {
      console.log(err);
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    var newForm = req.form.toJSON();
    //newForm.submissions = _submissions;

    if (req.userId) {
      if (req.form.admin._id + "" === req.userId + "") {
        return res.json(newForm);
      }
      return res.status(404).send({
        message: "Form Does Not Exist"
      });
    }
    return res.json(newForm);
  });
};

/**
 * Update a form
 */
exports.update = function(req, res) {
  var form = req.form;

  if (req.body.changes) {
    var formChanges = req.body.changes;

    formChanges.forEach(function(change) {
      diff.applyChange(form, true, change);
    });
  } else {
    //Unless we have 'admin' privileges, updating form admin is disabled
    if (req.user.roles.indexOf("admin") === -1) delete req.body.form.admin;

    //Do this so we can create duplicate fields
    var checkForValidId = new RegExp("^[0-9a-fA-F]{24}$");
    for (var i = 0; i < req.body.form.form_fields.length; i++) {
      var field = req.body.form.form_fields[i];
      if (!checkForValidId.exec(field._id + "")) {
        delete field._id;
      }
    }
    form = _.extend(form, req.body.form);
  }

  form.save(function(err, form) {
    if (err) {
      console.log(err);
      res.status(405).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(form);
    }
  });
};

/**
 * Delete a form
 */
exports.delete = function(req, res) {
  var form = req.form;
  // console.log('deleting form');
  Form.remove({ _id: form._id }, function(err) {
    if (err) {
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      // console.log('Form successfully deleted');
      // res.status(200).send('Form successfully deleted');
      res.json(form);
    }
  });
};

/**
 * Get All of Users' Forms
 */
exports.list = function(req, res) {
  //Allow 'admin' user to view all forms
  var searchObj = { admin: req.user };
  if (req.user.isAdmin()) searchObj = {};

  Form.find(searchObj)
    .sort("-created")
    .populate("admin.username", "admin._id")
    .exec(function(err, forms) {
      if (err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(forms);
      }
    });
};

/**
 * Form middleware
 */
exports.formByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: "Form is invalid"
    });
  } else {
    Form.findById(id)
      .populate("admin")
      .exec(function(err, form) {
        if (err) {
          return next(err);
        } else if (form === undefined || form === null) {
          res.status(404).send({
            message: "Form not found"
          });
        } else {
          //Remove sensitive information from User object
          var _form = form;
          _form.provider = undefined;

          req.form = _form;
          return next();
        }
      });
  }
};

/**
 * Form authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
  var form = req.form;
  if (
    req.form.admin.id !== req.user.id &&
    req.user.roles.indexOf("admin") === -1
  ) {
    res.status(403).send({
      message:
        "User " +
        req.user.username +
        " is not authorized to edit Form: " +
        form.title
    });
  }
  return next();
};
