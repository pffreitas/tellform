'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * FormStats Schema
 */
var FormStatsSchema = new Schema(
	{
		form: {
			type: Schema.Types.ObjectId,
			ref: 'Form',
			required: true
		},
		type: {
			type: String,
			default: ''
		}
	}, 
	{
		timestamps: { 
			createdAt: 'createdAt' 
		} 
	}
);

module.exports = mongoose.model('FormStats', FormStatsSchema);
