'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	crypto = require('crypto');

/**
 * Respondent Schema
 */
var RespondentSchema = new Schema({
	username: {
		type: String,
		unique: true,
		required: true
	},
	passwordHash: {
		type: String,
		default: ''
	},
	passwordText: {
		type: String,
		default: ''
	},
	salt: {
		type: String
	},
	formId: {
		type: String
	},
	hasResponded: {
		type: Boolean
	},
	started: {
		type: Boolean
	}
});


/**
 * Hook a pre save method to hash the password
 */
RespondentSchema.virtual('password').set(function (password) {
  	this.passwordHash = this.hashPassword(password);
});
RespondentSchema.virtual('password').get(function () {
  return this.passwordHash;
});


/**
 * Create instance method for hashing a password
 */
RespondentSchema.methods.hashPassword = function(password) {
	if(!this.salt){
		this.salt = crypto.randomBytes(64).toString('base64');
	}

	if (password) {
		return crypto.pbkdf2Sync(password, new Buffer(this.salt, 'base64'), 10000, 128, 'sha512').toString('base64');
	} else {
		return password;
	}
};


module.exports = mongoose.model('Respondent', RespondentSchema);
