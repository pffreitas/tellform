'use strict';

module.exports = {
	baseUrl: process.env.BASE_URL || 'http://localhost:3000',
	log: {
		format: 'dev',
	},
	sessionCookie: {
		domain: 'localhost'
	}
};
