"use strict";

// baseUrl: "http://www.perfixforms.com.br",

module.exports = {
  log: {
    format: "combined",
    options: {
      stream: "access.log"
    }
  },
  sessionCookie: {
    path: "/",
    httpOnly: true,
    secure: false,
    maxAge: 10800000
  }
};
