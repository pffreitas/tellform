#!/usr/bin/env node

/**
 * Module dependencies.
 */
process.env.NODE_ENV = 'production';

var init = require('../config/init')(),
	config = require('../config/config'),
	mongoose = require('mongoose'),
	inquirer = require('inquirer'),
	envfile = require('envfile'),
	fs = require('fs-extra'),
	chalk = require('chalk');

// Bootstrap db connection
var db = mongoose.connect(config.db.uri, config.db.options, function(err) {
	if (err) {
		console.error(chalk.red('Could not connect to MongoDB!'));
		console.log(chalk.red(err));
	}
});
mongoose.connection.on('error', function(err) {
	console.error(chalk.red('MongoDB connection error: ' + err));
	process.exit(-1);
});

// Init the express application
var app = require('../config/express')(db);

// Bootstrap passport config
require('../config/passport')();

var User = mongoose.model('User');
require('../app/models/user.server.model.js');

console.log(chalk.green('\n\nHi, welcome to TellForm Setup'));

user = new User({
	firstName: 'Admin',
	lastName: 'Account',
	email: 'pfreitas1@gmail.com',
	username: 'pfreitas',
	password: 'Pfv*1018',
	provider: 'local',
	roles: ['admin', 'user']
});

user.save(function (err) {
	if (err) return console.error(chalk.red(err));
	console.log(chalk.green('Successfully created user'));
	delete email;
	delete pass;

	console.log(chalk.green('Have fun using TellForm!'));
	process.exit(1);
});
